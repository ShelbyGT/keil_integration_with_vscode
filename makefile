build:
	@./Scripts/start_build.bat ${TARGET}

ALL_build:
	@./Scripts/start_ALL_build.bat

[DBG]_ALL_build:
	@./Scripts/start_DBG_ALL_build.bat

[REL]_ALL_build:
	@./Scripts/start_REL_ALL_build.bat

rebuild:
	@./Scripts/start_rebuild.bat ${TARGET}

ALL_rebuild:
	@./Scripts/start_ALL_rebuild.bat

[DBG]_ALL_rebuild:
	@./Scripts/start_DBG_ALL_rebuild.bat

[REL]_ALL_rebuild:
	@./Scripts/start_REL_ALL_rebuild.bat

ALL_clean:
	@./Scripts/start_ALL_clean.bat

program_flash:
	@./Scripts/start_program_flash.bat ${TARGET}