# Keil integration into VSCode and STM32CubeIDE

Bunch of scripts that integrate Keil's project and toolchain into VSCode and STM32CubeIDE.  
Also contains base folder's structure for Keil's project.   
It compiles project and parses errors/warnings/infos into VSCode and STM32CubeIDE.

For more information, visit following blog entries:  
[Keil integration into VSCode](https://www.edaboard.com/blog/keil-vscode-keil-integration-into-vscode.2103/)   
[Keil integration into STM32CubeIDE](https://www.edaboard.com/blog/keil-stm32cubeide-keil-integration-into-stm32cubeide.2116/)   

# Usage
1. Copy .vscode and scripts folders into the root folder of your Keil's project (with Src, Inc and perhaps MDK-ARM subfolder);
2. Open root folder of your Keil's project by "Open with Code" context menu; 

In VSCode:

3. Navigate to Keil's project file (.uvproj or .uvprojx) and open it;
4. Run "Run Build Task..." and choose from the list "Import settings from Keil's v4 or v5 project" - that will import includes, defines and output paths for all project's targets from opened project file;
5. Choose Keil's project target by "C/C++: Select a Configuration..." command in VSCode;
6. Run again "Run Build Task..." and choose desired feature, e.g. clean/build/rebuild/program.

# Available features  
### ![](overview.png)  
### Init
* __Import settings from Keil's v4 or v5 project__ - imports includes, defines and output paths for all project's targets
### Building
* __Build by Keil__ - builds Keil's project for a chosen target
* __ALL build by Keil__ - builds Keil's project for all targets (make sure that all targets have different output paths in Keil's project)
* __[DBG] ALL build by Keil__ - builds Keil's project for all targets that name starts with "[DBG]..."
* __[REL] ALL build by Keil__ - builds Keil's project for all targets that name starts with "[REL]..."
### Rebuilding
* __Rebuild by Keil__ - rebuilds Keil's project for a chosen target
* __[DBG] ALL rebuild by Keil__ - rebuilds Keil's project for all targets that name starts with "[DBG]..."
* __[REL] ALL rebuild by Keil__ - rebuilds Keil's project for all targets that name starts with "[REL]..."
* __ALL rebuild by Keil__ - rebuilds Keil's project for all targets (make sure that all targets have different output paths in Keil's project)
### Cleaning
* __ALL clean by Keil__ - cleans Keil's output files for all targets
### Programming
* __Program Flash by Keil__ - downloads the application to Flash using Keil's settings of a chosen target and available programming device
