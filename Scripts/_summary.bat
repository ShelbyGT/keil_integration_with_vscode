@echo off

echo( >> "%TMP_FOLDER%\program_size_summary.log"
set /A optim_level=%KEIL_PROJECT_OPTIM_LEVEL%-1
echo %date% %time%: %PROJECT_TARGET%
echo %date% %time%: %PROJECT_TARGET% >> "%TMP_FOLDER%\program_size_summary.log"
echo %PROJECT_TARGET%: Keil's compiler optimization: "Level %optim_level% (-O%optim_level%)"
echo %PROJECT_TARGET%: Keil's compiler optimization: "Level %optim_level% (-O%optim_level%)" >> "%TMP_FOLDER%\program_size_summary.log"
echo %~1
echo %~1 >> "%TMP_FOLDER%\program_size_summary.log"
findstr /C:"Grand Totals" "%PROJECT_FOLDER%\%KEIL_PROJECT_LISTING_DIR%*.map"
findstr /C:"Grand Totals" "%PROJECT_FOLDER%\%KEIL_PROJECT_LISTING_DIR%*.map" >> "%TMP_FOLDER%\program_size_summary.log"
findstr /C:"ROM Totals" "%PROJECT_FOLDER%\%KEIL_PROJECT_LISTING_DIR%*.map"
findstr /C:"ROM Totals" "%PROJECT_FOLDER%\%KEIL_PROJECT_LISTING_DIR%*.map" >> "%TMP_FOLDER%\program_size_summary.log"
findstr /C:"Total ROM Size" "%PROJECT_FOLDER%\%KEIL_PROJECT_LISTING_DIR%*.map"
findstr /C:"Total ROM Size" "%PROJECT_FOLDER%\%KEIL_PROJECT_LISTING_DIR%*.map" >> "%TMP_FOLDER%\program_size_summary.log"
findstr /C:"Load Region LR_IROM1" "%PROJECT_FOLDER%\%KEIL_PROJECT_LISTING_DIR%*.map" >> "%TMP_FOLDER%\program_size_summary.log"

python ".\Scripts\_program_size_summary_cf.py" "%TMP_FOLDER%\program_size_summary.log"

echo(
findstr /C:"Build time" "%TMP_FOLDER%\program_size_summary_cf.log"
findstr /C:"%PROJECT_TARGET%" "%TMP_FOLDER%\program_size_summary_cf.log"