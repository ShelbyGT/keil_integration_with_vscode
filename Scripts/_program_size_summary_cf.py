import argparse
import re
import os
import math

def parse_log_cf(log_path):
    bl_size = 0
    app_size = 0
    used_memory_size = 0
    max_size = 0
    total_memory_size = 0

    filename =  log_path
    ifile = open(filename, 'r')
    lines = ifile.readlines()
    ifile.close()

    filename_cf = os.path.splitext(filename)[0] + f'_cf.log'
    ofile = open(filename_cf, 'w')
    text = (f'Build time\tTarget\tBL Size [B]\tAPP Size [B]\tBL+APP Size [B]\tTotal FLASH Size [B]\tMemory used [%]\tMemory left [B]\n')
    for line in lines:
        build_time_search = re.search("(\d{4}-.*): ", line)
        target_name_search = re.search("^(.*): Keil's", line)
        app_size_search = re.search("^.*\.map:.*\)\s*(\d*) \(.*$", line)
        bl_max_size_search = re.search("Load Region LR_IROM1.*Base: 0x.{4}(\S+),.*Max: (0x\S+),", line)
        if build_time_search :
            text += f'{build_time_search.group(1)}\t'
        if target_name_search :
            text += f'{target_name_search.group(1)}\t'
        if app_size_search:
            app_size = int(app_size_search.group(1))
        if bl_max_size_search:
            bl_size = int(f'0x0000{bl_max_size_search.group(1)}',16)
            max_size = int(bl_max_size_search.group(2),16)
            total_memory_size = 2**math.ceil(math.log2(max_size + bl_size - 1))
            used_memory_size = bl_size + app_size
            text += f'{bl_size}\t'
            text += f'{app_size}\t'
            text += f'{used_memory_size}\t'
            text += f'{total_memory_size}\t'

            memory_used_percent = used_memory_size/total_memory_size * 100
            text += f'{memory_used_percent:.2f}\t'

            text += f'{total_memory_size - used_memory_size}\n'
    ofile.write(text)
    ofile.close()
  
if __name__ == "__main__":
    PARSER = argparse.ArgumentParser()
    PARSER.add_argument('log_path')
    APP_ARGS = PARSER.parse_args()
    APP_ARGS.log_path = APP_ARGS.log_path.replace("\\", "/")

    parse_log_cf(APP_ARGS.log_path)

    